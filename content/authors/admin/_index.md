---
# Display name
title: Hongguo Zhang 

# Full name (for SEO)
first_name: Hongguo
last_name: Zhang

# Is this the primary user of the site?
superuser: true

# Role/position/tagline
role: Ph.D. / Research Assistant

# Organizations/Affiliations to show in About widget
organizations:
  - name: School of Resources and Environment, University of Electronic Science and Technology of China
    url: https://sre.uestc.edu.cn/

# Short bio (displayed in user profile at end of posts)
bio: My research interests include spatial temporal data analysis, agricultural land use, time series remote sensing, deep learning.

# Interests to show in About widget
interests:
  - Artificial Intelligence
  - Spatial and Temporal Data Anaylsis
  - Information Retrieval

# Education to show in About widget
education:
  courses:
    - course: Ph.D. in Information and Communication Engineering
      institution: University of Electronic Science and Technology of China
      year: 2015-2022
    - course: B.S. in Spatial Information and Digital Technology

      institution: Chengdu University of Technology
      year: 2010-2014

# Social/Academic Networking
# For available icons, see: https://wowchemy.com/docs/getting-started/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
  - icon: envelope
    icon_pack: fas
    link: "mailto:hgzhang@uestc.edu.cn"
  - icon: researchgate
    icon_pack: ai
    link: https://www.researchgate.net/profile/Hongguo-Zhang
  - icon: graduation-cap # Alternatively, use `google-scholar` icon from `ai` icon pack
    icon_pack: fas
    link: https://scholar.google.com/citations?user=gTmgdnQAAAAJ
  - icon: github
    icon_pack: fab
    link: https://github.com/hgzhangs


# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/uploads/resume.pdf`, enable `ai` icons in `params.yaml`,
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: uploads/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: 'hgzhag@uestc.edu.cn'

# Highlight the author in author lists? (true/false)
highlight_name: true
---

Hongguo Zhang is a research assistant in at the Quantitative remote sensing team at the University of Electronic Science and Technology of China.
