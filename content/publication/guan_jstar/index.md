---
title: "Improving Land Cover Change Detection and Classification With BRDF Correction and Spatial Feature Extraction Using Landsat Time Series: A Case of Urbanization in Tianjin, China"
authors:
- Guan Yuwei
- Zhou Yanru
- He Binbin
- Liu Xiangzhuo
- admin
- Feng Shilei
author_notes:
  - ''
  - ''
  - 'Corresponding Author'

date: "2020-07-07"
doi: "10.1109/JSTARS.2020.3007562"

# Schedule page publish date (NOT publication's date).
publishDate: "2020-07-07"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*IEEE Journal of Selected Topics in Applied Earth Observations and Remote Sensing, 13*: 4166 - 4177"
publication_short: ""

abstract: As one of the important coastal cities in China, Tianjin has been urbanized dramatically over the past 40 years, and the urbanization rate has been up to 83.15% by 2018. In this study, we used the Continuous Change Detection and Classification (CCDC) algorithm to comprehensively understand the urban expansion processes in Tianjin based on the Landsat Time Series (LTS) from 1985 to 2018 with 30-meter resolution. Specially, we applied the c-factor approach with the RossThick-LiSparse-R model to correct the Bidirectional Reflectance Distribution Function (BRDF) effect for each Landsat image and calculated a spatial line-density feature for improving the change detection and the classification. Based on the study in Tianjin, we found that BRDF correction can substantially improve the change detection (9.00% higher overall accuracy) and classification (1.08% higher overall accuracy); and the line-density is also beneficial to classification (0.48% higher overall accuracy), especially for impervious surface (1.70% less commission errors and 1.49% less omission errors). By analyzing the imperious surface change processes, we observed that Tianjin has undergone rapid urban expansion in the past decades, and the urban area was mainly transfo

# Summary. An optional shortened abstract.
#summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
- BRDF
- CCDC
- Urbanization
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: https://ieeexplore.ieee.org/document/9134740/citations#citations
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
#image:
  #caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  #focal_point: ""
  #preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
