---
title: "Spatial and temporal patterns of rice planthopper populations in South and Southwest China"
authors:
- admin
- Binbin He
- Jin Xing
- Minghong Lu
author_notes:
  - ''
  - 'Corresponding Author'

date: "2022-02-11"
doi: "10.1016/j.compag.2022.106750"

# Schedule page publish date (NOT publication's date).
publishDate: "2022-02-11"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Computers and Electronics in Agriculture, 194*:106750"
publication_short: ""

abstract: Rice planthoppers (RPH) are important pest that cause severe yield losses of rice production in China. South and Southwest China are the main infestation areas on the annual spread of RPH. Identifying the spatial and temporal patterns in migration and subsequent development can provide insight into underlying mechanisms driving the spread of RPH and assist governments with prioritizing areas to achieve proactive management and prevention. Across rice production regions in South and Southwest China, RPH population were recorded by light traps and field surveys at 195 counties from March to October in 2000 to 2019. We first measured the spatial patterns in RPH immigrant populations and field populations with spatial autocorrelation analysis. Then, spatial hotspot analysis was undertaken to highlight dense RPH regions, and significant hot spots were further extracted for comparing the spatial and temporal patterns according to RPH species. The results revealed that RPH population were highly aggregated in space over large geographical distances up to 700 km approximately. Geographic patterns and hot spots of populations varied substantially with time and species, and various spatial patterns might be determined by inherent properties of RPH and rice eco-systematic. Overall, these results provided essential information to improving and optimizing the monitoring network for RPH. The findings from this research may provide helpful information to enhance proactive management against RPH and offer sustainable management practitioners new opportunities to design, develop, and implement optimal pest control strategies to protect rice production in China.

# Summary. An optional shortened abstract.
#summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
- Planthopper; spatial and temporal patterns
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: https://www.sciencedirect.com/science/article/pii/S0168169922000679?via%3Dihub
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
#image:
  #caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  #focal_point: ""
  #preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
