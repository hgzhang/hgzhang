---
title: "Deep spatial and temporal graph convolutional network for rice planthopper population dynamic forecasting"
authors:
- admin
- Binbin He
- Jin Xing
- Minghong Lu
author_notes:
  - ''
  - 'Corresponding Author'

date: "2023-05-05"
doi: "10.1016/j.compag.2023.107868 "

# Schedule page publish date (NOT publication's date).
publishDate: "2023-05-05"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Computers and Electronics in Agriculture, 210*:107868"
publication_short: ""

abstract: Rice planthoppers (RPH) are important pests that cause severe yield losses in rice production in China. The widespread and intensive use of insecticides to prevent RPH causes serious issues. Forecasting the population dynamics of RPH is an important part of integrated pest management, which helps to prevent impending RPH outbreaks and reduce the use of insecticide. However, forecasting RPH population dynamics is very challenging as it results from complex spatial and temporal dispersion processes impacted by many factors, such as source populations, meteorological conditions, and host plant characteristics. Therefore, a novel deep learning-based model using graph convolutional network (GCN) and long short-term memory network (LSTM) is proposed in this study to capture RPH population dynamics. This model includes one dynamic GCN (DGCN) for modeling source populations and two attention-based LSTM encoder-decoder network (ALSTM) for modeling meteorological and host plants, respectively. The proposed model can dynamically aggregate the outputs of one DGCN and two ALSTM based on data to produce the final output for each county. The proposed model is evaluated on the dataset collected in South and Southwest China during 2000–2019, which includes populations, meteorological and host plant factors. The results demonstrate that DGCN-ALSTM outperforms other state-of-art deep learning methods and traditional time series forecasting approaches. The improvement brought by DGCN-ALSTM are about 1.95 %, 1.32 %, and 1.60 % for brown planthopper population, and 1.79 %, 0.71 %, and 0.32 % for white back planthopper population according to the R, RMAE, and RRMSE take DGCN as the baseline, respectively. The proposed DGCN-ALSTM provides a new better tool to forecast RPH population dynamics and the accurate forecasting results can guide local plant protection organizations to develop detailed sustainable pest control strategies and methods to control RPH in time.

# Summary. An optional shortened abstract.
#summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
- Planthopper; GCN; Forecasting
featured: true

# links:
# - name: ""
#   url: ""
url_pdf: https://www.sciencedirect.com/science/article/pii/S0168169923002569?via%3Dihub
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
#image:
  #caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  #focal_point: ""
  #preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
