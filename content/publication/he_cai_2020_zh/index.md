---
title: "Design and implementation of a big data platform for precise application of rice pesticides and fertilizers"
authors:
- He Binbin
- Cao Hui
- admin
- Chen Jianhua
- Li Yanxi
- An Gangqiang
- Fan Chunquan
author_notes:
  - 'Corresponding Author'

date: "2020-01-25"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2020-01-05"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*China Agricultural Informatics, 32(1)*: 9-20"
publication_short: ""

abstract: ：［Purpose］To help precise application of pesticides and fertilizers in the process of rice planting，a big data platform named “Big data platform for precise application of rice pesticides and fertilizers” is designed and implemented，which is based on open source technology framework of big data and network.［Method］The platform requirements are sorted out through investigation and analysis，and functions and architecture of this platform are further designed. Firstly，the software and hardware environment of big data platform is built to meet the requirements of mass data storage（HDFS）and calculation（Spark），and the multi-source heterogeneous data related to application of rice pesticides and fertilizers are integrated. Then，the application and exploration of big data are carried out，including the establishment of expert knowledge base and the realization of core functions.［Result］Big data platform for precise application of rice pesticides and fertilizers can effectively reduce the use of pesticides and fertilizers and non-point source pollution in rice fields，and ensure food safety by integrating and innovating rice varieties with regional characteristic resistance and efficient utilization of pesticides and fertilizers and techniques for high efficient cultivation measures，reducing application of pesticides and fertilizers，and precision and light application of pesticides and fertilizers. Besides，it builds a network diversified promotion and demonstration platform and can be demonstrated and popularized on a large scale through scientific layout of demonstration and radiation rice area，integration of comprehensive technical model of reduction for application of pesticides and fertilizers，and giving full play to the leading role of new media + big data，the supporting role of scientific and technological talents of universities and agricultural academies. After the function test and pressure test，the platform can run normally and stably.［Conclusion］ Aiming at the application scenarios of precise application of rice pesticides and fertilizers，big data technology has played an irreplaceable role in integration，storage，processing，and application. It can be further combined with internet technology，geographic information system， global positioning system，and remote sensing technology in practical applications. However， the platform is put into use for a short time，so it is necessary to continuously accumulate data to enrich its practicality.



# Summary. An optional shortened abstract.
#summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
- BRDF
- CCDC
- Urbanization
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: ''
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
#image:
  #caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  #focal_point: ""
  #preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
