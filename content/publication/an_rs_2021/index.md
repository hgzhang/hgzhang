---
title: "Extraction of Areas of rice false smut infection using UAV hyperspectral data"
authors:
- An Gangqiang
- Xing Minfeng
- He Binbin
- Kang Haiqi
- Shang Jiali
- Liao Chunhua
- Huang Xiaodong
- admin
author_notes:
  - ''
  - 'Corresponding Author'

date: "2021-08-11"
doi: "10.3390/rs13163185"

# Schedule page publish date (NOT publication's date).
publishDate: "2021-08-11"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Remote Sensing, 13(16)*:3185"
publication_short: ""

abstract: Rice false smut (RFS), caused by Ustilaginoidea virens, is a significant grain disease in rice that can lead to reduced yield and quality. In order to obtain spatiotemporal change information, multitemporal hyperspectral UAV data were used in this study to determine the sensitive wavebands for RFS identification, 665–685 and 705–880 nm. Then, two methods were used for the extraction of rice false smut-infected areas, one based on spectral similarity analysis and one based on spectral and temporal characteristics. The final overall accuracy of the two methods was 74.23 and 85.19%, respectively, showing that the second method had better prediction accuracy. In addition, the classification results of the two methods show that the areas of rice false smut infection had an expanding trend over time, which is consistent with the natural development law of rice false smut, and also shows the scientific nature of the two methods.

# Summary. An optional shortened abstract.
#summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
- Rice false smunt
- UAV
- hyperspectral
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: https://www.mdpi.com/2072-4292/13/16/3185
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
#image:
  #caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  #focal_point: ""
  #preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
