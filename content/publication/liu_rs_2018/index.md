---
title: "Near Real-Time Extracting Wildfire Spread Rate from Himawari-8 Satellite Data"
authors:
- Liu Xiangzhuo
- He Binbin
- Quan Xingwen
- Yebra Marta
- Shi Qiu
- Yin Changming
- Liao Zhanmang
- admin
author_notes:
  - ''
  - 'Corresponding Author'

date: "2018-09-05"
doi: "10.3390/rs10101654"

# Schedule page publish date (NOT publication's date).
publishDate: "2018-09-05"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["2"]

# Publication name and optional abbreviated publication name.
publication: "*Remote Sensing, 10(10)*:1654"
publication_short: ""

abstract: Fire Spread Rate (FSR) can indicate how fast a fire is spreading, which is especially helpful for wildfire rescue and management. Historically, images obtained from sun-orbiting satellites such as Moderate Resolution Imaging Spectroradiometer (MODIS) were used to detect active fire and burned area at the large spatial scale. However, the daily revisit cycles make them inherently unable to extract FSR in near real­-time (hourly or less). We argue that the Himawari-8, a next generation geostationary satellite with a 10-min temporal resolution and 0.5–2 km spatial resolution, may have the potential for near real-time FSR extraction. To that end, we propose a novel method (named H8-FSR) for near real-time FSR extraction based on the Himawari-8 data. The method first defines the centroid of the burned area as the fire center and then the near real-time FSR is extracted by timely computing the movement rate of the fire center. As a case study, the method was applied to the Esperance bushfire that broke out on 17 November, 2015, in Western Australia. Compared with the estimated FSR using the Commonwealth Scientific and Industrial Research Organization (CSIRO) Grassland Fire Spread (GFS) model, H8-FSR achieved favorable performance with a coefficient of determination (R2) of 0.54, mean bias error of –0.75 m/s, mean absolute percent error of 33.20% and root mean square error of 1.17 m/s, respectively. These results demonstrated that the Himawari-8 data are valuable for near real-time FSR extraction, and also suggested that the proposed method could be potentially applicable to other next generation geostationary satellite data.

# Summary. An optional shortened abstract.
#summary: Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis posuere tellus ac convallis placerat. Proin tincidunt magna sed ex sollicitudin condimentum.

tags:
- FSR
- Himawari-8
featured: false

# links:
# - name: ""
#   url: ""
url_pdf: https://www.mdpi.com/2072-4292/13/16/3185
url_code: ''
url_dataset: ''
url_poster: ''
url_project: ''
url_slides: ''
url_source: ''
url_video: ''

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder. 
#image:
  #caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/jdD8gXaTZsc)'
  #focal_point: ""
  #preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects: []

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
# slides: example
---
