---
# An instance of the Contact widget.
widget: contact

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 40

title: Contact
subtitle:

content:
  # Automatically link email and phone or display as text?
  autolink: true



  # Contact details (edit or remove options as required)
  email: hgzhang@uestc.edu.cn
  address:
    street: 2006 Xiyuan Ave
    city: Chengdu
    region: West Hi-Tech Zone.
    postcode: 611731
    country: China
    country_code: CN
  coordinates:
    latitude: '37.4275'
    longitude: '-122.1697'
  directions:  Innovation Center B530
  # office_hours:
  #   - 'Monday 10:00 to 13:00'
  #   - 'Wednesday 09:00 to 10:00'
  # appointment_url: 'https://calendly.com'
design:
  columns: '2'
---
